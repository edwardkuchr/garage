﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using GarageGuard.Hubs;
using GarageGuard.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;

/// <summary>
/// Это создал при создании webApi
/// </summary>
namespace GarageGuard.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GuardsController : ControllerBase
    {

        private readonly IHubContext<ChatHub> _chatHubContext;
        private readonly GarageGuard.Data.GarageGuardContext _context;
        private readonly ILogger _logger; // создаем журнал лога
        public class LoggingEvents  //класс идентификаторов лога
        {
            public const int Get = 1001;
            public const int Post = 1002;
            public const int Put = 1003;
            public const int Del = 1004;
        }
        public GuardsController(GarageGuard.Data.GarageGuardContext context, IHubContext<ChatHub> chatHubContext, ILogger<GuardsController> logger)
          {
            _context = context;
            _chatHubContext = chatHubContext;
            _logger = logger;

        }
      

        // GET api/guard
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Guard>>> Get()
         {
            try
            {
               
                _logger.LogInformation(LoggingEvents.Get, $"Message displayed: API запрос к методу Get() {DateTime.UtcNow.ToLongTimeString()}"); //Log

                GarageGuard.Pages.Guards.IndexModel.timeGetToController = DateTime.UtcNow.AddHours(5); //при запросах от контроллера, пропишем текущую дату и время.
                await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "timeGetToController", GarageGuard.Pages.Guards.IndexModel.timeGetToController.ToString("G")); // обновим данные у всех клиентов
             

                return await _context.Guard.ToListAsync();
               
            
           
            }
            catch (Exception ex)
            {
               _logger.LogError(LoggingEvents.Get, $"Message displayed: Ошибка в Api/GetAll():{ex.Message} {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return null;
            }
        }

        // POST api/guard

        
        [HttpPost]
        public async Task<ActionResult<Guard>> Post(Guard guard)
        {
            try
            {
                _context.Guard.Add(guard);
                await _context.SaveChangesAsync();

                _logger.LogInformation(LoggingEvents.Post, $"Message displayed: API запрос к методу Post() -Ok {DateTime.UtcNow.ToLongTimeString()}"); //Log

                return Ok(guard);

            }

            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.Post, $"Message displayed: Ошибка в Api/Post():{ex.Message} {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return null;
            }
        }
        // PUT api/guard/
        [HttpPut]
        public async Task<ActionResult<Guard>> Put(Guard guard)
        {
            try
            {
                if (guard == null)
                {
                    _logger.LogError(LoggingEvents.Put, $"Message displayed: API запрос к методу Put() -BadRequest {DateTime.UtcNow.ToLongTimeString()}"); //Log
                    return BadRequest();
                }
                if (!_context.Guard.Any(x => x.ID == guard.ID))
                {
                    _logger.LogError(LoggingEvents.Put, $"Message displayed: API запрос к методу Put() -NotFound {DateTime.UtcNow.ToLongTimeString()}"); //Log
                    return NotFound();
                }
                //отправим соощение всем клиентам
                if (!_context.Guard.Any(x => x.Status == guard.Status)) //если в базе данных Status отличается от нового значения 
                {
                    await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "status", guard.Status);
                }
                if (!_context.Guard.Any(x => x.MainsVoltage == guard.MainsVoltage)) //если в базе данных mainsVoltage отличается от нового значения 
                {
                    await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "mainsVoltage", guard.MainsVoltage);
                }
                if (!_context.Guard.Any(x => x.BackupPower == guard.BackupPower)) //если в базе данных BackupPower отличается от нового значения 
                {
                    await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "backupPower", guard.BackupPower);
                }
                if (!_context.Guard.Any(x => x.Motion == guard.Motion)) //если в базе данных motion отличается от нового значения 
                {
                    await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "motion", guard.Motion, "state", guard.State);
                    
                }
                if (!_context.Guard.Any(x => x.Temperature == guard.Temperature)) //если в базе данных temperature отличается от нового значения 
                {
                    await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "temperature", guard.Temperature);
                }
                if (!_context.Guard.Any(x => x.Humidity == guard.Humidity)) //если в базе данных humidity отличается от нового значения 
                {
                    await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "humidity", guard.Humidity);
                }
                if (!_context.Guard.Any(x => x.State == guard.State)) //если в базе данных Статус охраны отличается от нового значения 
               {
                    await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "state", guard.State);
                }
                if (!_context.Guard.Any(x => x.relay_1 == guard.relay_1)) //если в базе данных Статус реле отличается от нового значения 
                {
                    await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "relay_1", guard.relay_1);
                }
                DateTime time= DateTimeOffset.UtcNow.AddHours(5).DateTime; //пропишем текущее время
                guard.Date = time;
                await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "timeGetFromController", time.ToString("G"));
                await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "statusLine", false); // запустим бегущую строку


                _context.Update(guard);
                await _context.SaveChangesAsync();
                Pages.Guards.IndexModel._awaitExchange = false; // после получение запроса от контроллера, отменим ожидание синхронизации и запустим бегущую строку

                _logger.LogInformation(LoggingEvents.Put, $"Message displayed: API запрос к методу Put() -Ok {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return Ok(guard);
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.Put, $"Message displayed: Ошибка в Api/Put():{ex.Message} {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return null;
            }
        }
        // DELETE api/guard/8
        [HttpDelete("{id}")]
        public async Task<ActionResult<Guard>> Delete(int id)
        {
            try
            {
                Guard guard = _context.Guard.FirstOrDefault(x => x.ID == id);
                if (guard == null)
                {
                    _logger.LogError(LoggingEvents.Del, $"Message displayed: Ошибка в Api/Delete(): -Not Found {DateTime.UtcNow.ToLongTimeString()}"); //Log
                    return NotFound();
                }
                _context.Guard.Remove(guard);
                await _context.SaveChangesAsync();
                _logger.LogInformation(LoggingEvents.Del, $"Message displayed: API запрос к методу Delete() -Ok {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return Ok(guard);
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.Del, $"Message displayed: Ошибка в Api/Delete():{ex.Message} {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return null;
            }
        }

    }
}
