﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GarageGuard.Data;
using GarageGuard.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using GarageGuard.Areas.Identity.Pages.Account;
using Microsoft.AspNetCore.Authorization;

namespace GarageGuard.Pages.Guards
{
    [Authorize(Roles = "Admin")]

    public class DeleteModel : PageModel
    {
      

        private readonly GarageGuard.Data.GarageGuardContext _context;
        private readonly ILogger _logger; //создаем журнал лога

        public DeleteModel(GarageGuard.Data.GarageGuardContext context, ILogger<AdminPanelModel> logger)
        {
            _context = context;
            _logger = logger;
        }

        [BindProperty]
        public Guard Guard { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                Guard = await _context.Guard.FirstOrDefaultAsync(m => m.ID == id);

                if (Guard == null)
                {
                    return NotFound();
                }
                return Page();
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Message displayed: Ошибка при отркытии страницы удаления экземмпляра Guard: {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return RedirectToPage("./");//вернемся на главную страницу
            }
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                Guard = await _context.Guard.FindAsync(id);

                if (Guard != null)
                {
                  
                        _context.Guard.Remove(Guard);
                        await _context.SaveChangesAsync();
                        _logger.LogInformation($"Message displayed: Экземпляр Guard удален: {DateTime.UtcNow.ToLongTimeString()}"); //Log

                    
                }

                return RedirectToPage("./");
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Message displayed: Ошибка при удалении экземпляра Guard: {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return RedirectToPage("./");//вернемся на главную страницу
            }
        }
    }
}
