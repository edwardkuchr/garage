﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GarageGuard.Data;
using GarageGuard.Models;
using Microsoft.AspNetCore.SignalR;
using GarageGuard.Hubs;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using GarageGuard.Utility;

namespace GarageGuard.Pages.Guards
{
   // [Authorize (Roles="admin")]  // страница доступна только для авторизованных пользователей
    public class IndexModel : PageModel
    {
        public static bool _awaitExchange; // переменная для уведомлении о синхронизации по которой определяем какой стиль у бегущей строки 

        public class LoggingEvents  //класс идентификаторов лога
        {
            public const int ActivationGuard = 2000;  /// нажатие кнопки влючения охраны
            public const int ActivationRelay = 3000;  /// нажатие кнопки влючения выключения реле
        }

        
        private readonly GarageGuard.Data.GarageGuardContext _context;
        private readonly ILogger _logger; //создаем журнал лога
        private readonly IHubContext<ChatHub> _chatHubContext;  //hub SingnalR
        public SD sd; // класс с ролями
        public IndexModel(GarageGuard.Data.GarageGuardContext context, ILogger<IndexModel> logger, IHubContext<ChatHub> chatHubContext)
        {
            
            _context = context;
            _logger = logger;
            _chatHubContext = chatHubContext;
        }


        public static DateTimeOffset timeGetToController;// дата и время фиксируемое при обмене данными с контроллером. 

        public IList<Guard> Guard { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            try
            {
                Guard = await _context.Guard.ToListAsync();
                //добавим лог при каждом посещении страницы

                _logger.LogInformation($"Message displayed: Открыта главаная страница: {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return Page();
            }
            catch (Exception ex)
            {
                _logger.LogError( $"Message displayed: Ошибка при открытии главной страницы:{ex.Message} {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return NotFound();
            }

        }
       
        /// <summary>
        /// Обработчик включение\выключения охраны
        /// </summary>
        public async  Task<RedirectToPageResult> OnPostActivationGuard()
        {
            try
            {
                Guard = await _context.Guard.ToListAsync();
                Guard.LastOrDefault().State = !Guard.LastOrDefault().State; // инверсия значения
               
                Guard.LastOrDefault().Status = "Ожидание обмена"; //ждем синхронизации с контроллером
               
                await _context.SaveChangesAsync();
                // // при нажатии кнопки активации охраны, отправить всем клиентам новый статус
                await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "state", Guard.LastOrDefault().State);
                _awaitExchange = true;
                await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "status", Guard.LastOrDefault().Status);//ждем синхронизации с контроллером
                await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "statusLine", _awaitExchange);  // остановим бегущую строку);

                _logger.LogInformation(LoggingEvents.ActivationGuard, $"Message displayed: Нажата кнопка активации охраны: {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return RedirectToPage("./Index"); //вернемся на главную страницу
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.ActivationGuard, $"Message displayed: Ошибка при нажатии кнопки включения:{ex.Message} {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return RedirectToPage("./Index");//вернемся на главную страницу
            }
        }
        /// <summary>
        /// Обработчик включения реле
        /// </summary>
        /// <returns></returns>
        public async Task<RedirectToPageResult> OnPostActivationRelay()
        {
            try
            {
                Guard = await _context.Guard.ToListAsync();
                Guard.LastOrDefault().relay_1 = !Guard.LastOrDefault().relay_1; // инверсия значения

                _awaitExchange = true;
                Guard.LastOrDefault().Status = "Ожидание обмена"; //ждем синхронизации с контроллером
               
                await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "status", Guard.LastOrDefault().Status);//ждем синхронизации с контроллером
               

                await _context.SaveChangesAsync();
                // // при нажатии кнопки включения реле, отправить всем клиентам новый статус
                await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "relay_1", Guard.LastOrDefault().relay_1);
                 await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "statusLine",_awaitExchange);  // остановим бегущую строку);
               // await _chatHubContext.Clients.All.SendAsync("ReceiveMessage", "status", "Ожидание обмена");  // при нажатии кнопки ожидание связи с контроллером
                _logger.LogInformation(LoggingEvents.ActivationRelay, $"Message displayed: Нажата кнопка вкл/откл реле: {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return RedirectToPage("./Index"); //вернемся на главную страницу
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.ActivationRelay, $"Message displayed: Ошибка при нажатии кнопки вкл/откл реле:{ex.Message} {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return RedirectToPage("./Index");//вернемся на главную страницу
            }
        }

    }
}
