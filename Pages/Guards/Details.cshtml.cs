﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GarageGuard.Data;
using GarageGuard.Models;

namespace GarageGuard.Pages.Guards
{
    public class DetailsModel : PageModel
    {
        private readonly GarageGuard.Data.GarageGuardContext _context;

        public DetailsModel(GarageGuard.Data.GarageGuardContext context)
        {
            _context = context;
        }

        public Guard Guard { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Guard = await _context.Guard.FirstOrDefaultAsync(m => m.ID == id);

            if (Guard == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
