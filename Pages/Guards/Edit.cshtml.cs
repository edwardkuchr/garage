﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GarageGuard.Data;
using GarageGuard.Models;
using Microsoft.AspNetCore.Authorization;

namespace GarageGuard.Pages.Guards
{
    [Authorize(Roles = "Admin")]
    public class EditModel : PageModel
    {
        private readonly GarageGuard.Data.GarageGuardContext _context;

        public EditModel(GarageGuard.Data.GarageGuardContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Guard Guard { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Guard = await _context.Guard.FirstOrDefaultAsync(m => m.ID == id);

            if (Guard == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Guard).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GuardExists(Guard.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool GuardExists(int id)
        {
            return _context.Guard.Any(e => e.ID == id);
        }
    }
}
