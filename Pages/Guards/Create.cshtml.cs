﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using GarageGuard.Data;
using GarageGuard.Models;
using Microsoft.AspNetCore.Authorization;

namespace GarageGuard.Pages.Guards
{
    [Authorize(Roles = "Admin")]
    public class CreateModel : PageModel
    {
        private readonly GarageGuard.Data.GarageGuardContext _context;

        public CreateModel(GarageGuard.Data.GarageGuardContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Guard Guard { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Guard.Add(Guard);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
