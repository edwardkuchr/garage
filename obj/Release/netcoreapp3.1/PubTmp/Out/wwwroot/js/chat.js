﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();

//Disable send button until connection is established
//document.getElementById("sendButton").disabled = true;


//метод передачи параметра со значением
connection.on("ReceiveMessage", function (param, value) {
    // var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    // var encodedMsg = user + " says " + msg;
    //  var li = document.createElement("li");
    //  li.textContent = encodedMsg;
  

    if (param == "state")   // если передается значение статуса охраны (state)
    {
        if (value == false)
        {
            document.getElementById("btnActivationGuard").style.backgroundColor = "red";//setAttribute("disabled", true);
            document.getElementById("btnActivationGuard").textContent = "Охрана выключена";
        }
        else
        {
            document.getElementById("btnActivationGuard").style.backgroundColor = "green";
            document.getElementById("btnActivationGuard").textContent = "Охрана включена";
        }
    }
   

    if (param == "relay_1")   // если передается значение статуса реле (relay)
    {
        if (value == false)
        {
            document.getElementById("btnActivationRelay").style.backgroundColor = "red";//setAttribute("disabled", true);
            document.getElementById("btnActivationRelay").textContent = "Реле-1 выключено";
        }
        else
        {
            document.getElementById("btnActivationRelay").style.backgroundColor = "green";
            document.getElementById("btnActivationRelay").textContent = "Реле-1 включено";
        }
    }
    ///<summary> ниже оперция обновляет значения показаний на странице. Это нужно делать после условии с кнопками. Иначе в эксплорере не будут
    /// обновляться значения. В опере независимо от нахождения всегда работает.
    /// </summary>
    /// <returns></returns>
    if (param == "temperature1") // тестовый код, на текущий момент не используется
    {
        let d = document.getElementById("temperature");
       // var backColor = window.getComputedStyle(d).backgroundColor;
      //  var fontColor = window.getComputedStyle(d).color;
        let styl = d.style.cssText;
        d.className = 'tbody-parameters';
       // d.style.backgroundColor = "red";
      //  d.style.color = "white";
        setInterval(function () { d.className = 'none';d.style.cssText = styl;}, 500);
        
   //     document.getElementById("tbody-temp").style.backgroundColor = "red";
    }

    document.getElementById(param).textContent = value; //тут обновляется значения параметров на странице

    let d = document.getElementById(param);    // тут визуализация изменения данных на странице
    let styl = d.style.cssText;
    //d.className = 'none';
    d.className = 'tbody-parameters';  
    setInterval(function () { d.className = 'none'; d.style.cssText = styl; }, 500);

     
});




/*
connection.on("ReceiveNotification", function (user, message) {

    // Show the notification.
 //   document.getElementById("messagesList").textContent = message;//("avevev");//.appendChild(li);
});
*/
connection.start();/*.then(function () {
    document.getElementById("sendButton").disabled = false;
}).catch(function (err) {
    return console.error(err.toString());
}); */
//connection.invoke("SendMessage", "user", "message"); 

/*

document.getElementById("sendButton").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    connection.invoke("SendMessage", user, message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault(); 
});*/
