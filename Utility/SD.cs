﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/// <summary>
/// Класс создан для присвоение ролей при регистрации нового пользователя
/// </summary>
namespace GarageGuard.Utility
{
    public class SD
    {
        public const string AdminEndUser = "Admin";
        public const string CustomerEndUser = "Customer";
    }
}
