using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using GarageGuard.Utility;
using Microsoft.AspNetCore.Authorization;

namespace GarageGuard.Areas.Identity.Pages.Account
{
    [Authorize(Roles = "Admin")]
    public class AdminPanelModel : PageModel
    {
        public class LoggingEvents  //����� ��������������� ����
        {
            // public const int ActivationGuard = 2000;  /// ������� ������ �������� ������
            // public const int ActivationRelay = 3000;  /// ������� ������ �������� ���������� ����
        }

        private readonly GarageGuard.Data.GarageGuardContext _context;
        private readonly ILogger _logger; //������� ������ ����


        public AdminPanelModel(GarageGuard.Data.GarageGuardContext context, ILogger<AdminPanelModel> logger)
        {
            _context = context;
            _logger = logger;
        }


        
        public IList<IdentityUser> Users { get; set; } // ������� �������������
        public IList<IdentityUserRole<string>> IdRoles { get; set; } // ������� ���������� �� ����� � �� �������������
        public IList<IdentityRole> Roles { get; set; } // ������� �������� �����

        public IList<SelectListItem> Options { get; set; } //������ ���� ��� ����������� �� ��������

        public async Task<IActionResult> OnGetAsync()
        {
            try
            {
                Users = await _context.Users.ToListAsync();
                IdRoles = await _context.UserRoles.ToListAsync();
                Roles = await _context.Roles.ToListAsync();
               

                _logger.LogInformation($"Message displayed: ������� ������ ��������������: {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return Page();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message displayed: ������ ��� �������� ������ ��������������:{ex.Message} {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return RedirectToPage("./");//�������� �� ������� ��������
            }

        }     
    }
    }
