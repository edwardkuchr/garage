using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GarageGuard.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;



namespace GarageGuard.Areas.Identity.Pages.Account
{
    [Authorize(Roles = "Admin")]
    public class ChangeRoleModel : PageModel
    {

        private readonly GarageGuard.Data.GarageGuardContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager; //������� �������� �����
        private readonly ILogger<RegisterModel> _logger;

        public ChangeRoleModel(GarageGuard.Data.GarageGuardContext context, UserManager<IdentityUser> userManager, ILogger<RegisterModel> logger, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
            _logger = logger;
        }

        [BindProperty]
        public IdentityUser _identityUser { get; set; }
        public IList<IdentityUser> Users { get; set; } // ������� �������������
        public IList<IdentityUserRole<string>> IdRoles { get; set; } // ������� ���������� �� ����� � �� �������������
        public IList<IdentityRole> Roles { get; set; } // ������� �������� �����

        public IList<SelectListItem> Options { get; set; } //������ ���� ��� ����������� �� ��������


        public async Task<IActionResult> OnGetAsync(string? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                _identityUser = await _userManager.FindByIdAsync(id);
                Users = await _context.Users.ToListAsync();
                IdRoles = await _context.UserRoles.ToListAsync();
                Roles = await _context.Roles.ToListAsync();
                Options = _context.Roles.Select(a => // �������� ������ � �������
                               new SelectListItem
                               {
                                   Value = a.Id.ToString(),
                                   Text = a.Name
                               }).ToList();

                if (_identityUser == null)
                {
                    return NotFound();
                }
                _logger.LogInformation($"Message displayed: �������� ��������� ���� � ������������: {_identityUser.UserName}: {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return Page();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message displayed: ������ ��� �������� �������� ��������� ���� � ������������: {ex.Message} {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return RedirectToPage("./AdminPanel");//�������� �� ������� ��������
            }

        }
        public async Task<IActionResult> OnPostAsync(string id, string role)//, string roleName) // ����� ����
        {
            try
            {
                if (id== null && role==null)
                {
                    return NotFound();
                }
                Roles = await _context.Roles.ToListAsync();
                Users = await _context.Users.ToListAsync();
                IdRoles = await _context.UserRoles.ToListAsync();
                role = Roles.First(y => y.Id == role).Name; //������������� �� ���� � ���
                _identityUser = await _userManager.FindByIdAsync(id);
                /// �������������� ���� �� ������ � ��������� �� SD
                if (role == "Admin")
                { role = SD.AdminEndUser; }
                else
                { role = SD.CustomerEndUser; }
                                 

                if (_identityUser != null)
                {
                    int num=0; // ��������� ���������� ������������� � ����� ������, ���� �� ������� ���� � ������������� ������
                    foreach (var item in IdRoles)
                    {
                        if (item.RoleId == Roles.First(x => x.Name == SD.AdminEndUser).Id)
                        { num++; }
                    }
                   
                    if (num <= 1 && role == SD.CustomerEndUser) // ���� � ���� ������� ������ ���� �������������, �� ��� ������ ������� ���� �� Customer
                    {
                        _logger.LogInformation($"Message displayed: ������ ���� ������ �����, ��� ��� ��� ������������ �����: {_identityUser.UserName}: {DateTime.UtcNow.ToLongTimeString()}"); //Log
                    }
                    else
                    {
                        // ������� ������ ��������� ����� ������������
                        List<string> userRole = (List<string>)await _userManager.GetRolesAsync(_identityUser);
                        // �������� �� ���������� �� ��� ������ ���� � �������������
                        if (userRole.Contains(role) == false)
                        {
                            //������ ������� ������������ ����� ����
                            var result = await _userManager.AddToRoleAsync(_identityUser, role);
                            if (result.Succeeded == true)
                            {
                                //������ ������ ���������� ��� ����
                                foreach (var item in userRole)
                                {
                                    if (item != role)
                                    {
                                        await _userManager.RemoveFromRoleAsync(_identityUser, item);
                                    }
                                }

                            }
                        }
                        _logger.LogInformation($"Message displayed: ����� ���� � ������������: {_identityUser.UserName}: {DateTime.UtcNow.ToLongTimeString()}"); //Log
                    }
                    
                }
                

                return RedirectToPage("./AdminPanel");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message displayed: ������ ��� ����� ���� � ������������: {ex.Message} {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return RedirectToPage("./AdminPanel");//�������� �� ������� ��������
            }
        }

    }
}
