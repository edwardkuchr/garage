using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GarageGuard.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
#nullable enable

namespace GarageGuard.Areas.Identity.Pages.Account
{
    [Authorize(Roles = "Admin")]
    public class UserDeleteModel : PageModel
    {

        private readonly GarageGuard.Data.GarageGuardContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        public IList<IdentityUser> Users { get; set; } // ������� �������������
        public IList<IdentityUserRole<string>> IdRoles { get; set; } // ������� ���������� �� ����� � �� �������������
        public IList<IdentityRole> Roles { get; set; } // ������� �������� �����

        public UserDeleteModel(GarageGuard.Data.GarageGuardContext context, UserManager<IdentityUser> userManager, ILogger<RegisterModel> logger)
        {
            _context = context;
            _userManager = userManager;
            _logger = logger;
        }

        [BindProperty]
       public IdentityUser _identityUser { get; set; }
      

        public async Task<IActionResult> OnGetAsync(string? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                _identityUser = await _userManager.FindByIdAsync(id);

                if (_identityUser == null)
                {
                    return NotFound();
                }
                _logger.LogInformation($"Message displayed: �������� ��������  ������������� �������� ������������: {_identityUser.UserName}: {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return Page();                
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message displayed: ������ ��� �������� �������� ������������� �������� ������������: {ex.Message} {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return RedirectToPage("./AdminPanel");//�������� �� ������� ��������
            }

        }
        public async Task<IActionResult> OnPostAsync(string? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                _identityUser = await _userManager.FindByIdAsync(id);


                if (_identityUser != null)
                {
                    Users = await _context.Users.ToListAsync();
                    IdRoles = await _context.UserRoles.ToListAsync();
                    Roles = await _context.Roles.ToListAsync();
                   
                    int num = 0; // ��������� ���������� ������������� � ����� ������, ���� �� ������� ������������� ������
                    foreach (var item in IdRoles)
                    {
                        if (item.RoleId == Roles.First(x => x.Name == SD.AdminEndUser).Id)
                        { num++; }
                    }
                    if (Roles.First(y => y.Id == IdRoles.First(x => x.UserId == id.ToString()).RoleId).Name == "Admin" && num<=1)
                    {
                        _logger.LogInformation($"Message displayed: ������ ������� ������� ������������, ��� ��� �� ��������� �����: {DateTime.UtcNow.ToLongTimeString()}"); //Log
                    }
                    else
                    {
                        await _userManager.DeleteAsync(_identityUser);
                        await _context.SaveChangesAsync();
                        _logger.LogInformation($"Message displayed: ������ ������������: {_identityUser.UserName}: {DateTime.UtcNow.ToLongTimeString()}"); //Log

                       
                    }
                }
                return RedirectToPage("./AdminPanel");
            }
            catch (Exception ex)
            {
                _logger.LogError( $"Message displayed: ������ ��� �������� ������������: {ex.Message} {DateTime.UtcNow.ToLongTimeString()}"); //Log
                return RedirectToPage("./AdminPanel");//�������� �� ������� ��������
            }
        }

    }
}
