﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageGuard.Models
{
    public class Guard
    {
        public int ID { get; set; } // ИД в базе данных (первичный ключ)
        public string Status { get; set; } // статусное сообщение состояие охраны или лог

        [DataType(DataType.Date)]
        public DateTime Date { get; set; } // дата обновления записи
        public double MainsVoltage { get; set; } // напряжение в сети 220 вольт
        public double BackupPower { get; set; } // напряжение в резервном питании
        public bool State { get; set; } // статус состояние охраны
        public int Motion { get; set; } //количество сработок датчика движения

        public double Temperature { get; set; } //  температура на датчике

        public double Humidity { get; set; } // влажность с датчика

        public bool relay_1 { get; set; } // состояние реле 1

        public int MissedCals { get; set; }// пропущенные звонки на SIM

        public string LastInSms { get; set; } // последнее входящее смс

        public bool WifiStatus { get; set; }// есть ли интернет через интернет
    }
}
