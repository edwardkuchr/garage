﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using GarageGuard.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace GarageGuard.Data
{
    public class GarageGuardContext : IdentityDbContext
    {
        public GarageGuardContext (DbContextOptions<GarageGuardContext> options)
            : base(options)
        {
            Database.EnsureCreated();   // создаем базу данных при первом обращении
        }

        public DbSet<GarageGuard.Models.Guard> Guard { get; set; }

      

      
    }
}
