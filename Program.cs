using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;


namespace GarageGuard
{
    public class Program
    {
        public static void Main(string[] args)
        {
            
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .ConfigureLogging((hostingContext,logging) =>  // ������������ �����������
            {
                logging.ClearProviders();
                // Requires `using Microsoft.Extensions.Logging;`
                //logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                logging.AddConsole();
             //   logging.AddDebug();
               // logging.AddEventSourceLogger();

            })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls("http://192.168.1.191:5000"); // ���� ������� Kestrel
                });

    }
}
